package org.store.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.store.dto.ArticleDto;
import org.store.dto.DiscountRuleDto;
import org.store.dto.response.GetDisplayItemsResponse;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {StoreServiceImpl.class})
public class StoreServiceImplTest {

    @MockBean private ArticleServiceImpl articleService;
    @MockBean private DiscountServiceImpl discountService;

    @Autowired private StoreServiceImpl storeService;

    @Test
    public void testGetDisplayItems_Successfully() {
        // GIVEN
        long purchaseDate = 1642957200000L; // 2022/01/24

        List<ArticleDto> articleDtoList = new ArrayList<>();
        ArticleDto shoes = new ArticleDto();
        shoes.setName("Shoes1");
        shoes.setSlogan("Best shoes");
        shoes.setNetPrice(100);
        shoes.setSalesPrice(150);
        shoes.setVatRatio(0.1f);
        articleDtoList.add(shoes);

        List<DiscountRuleDto> discountRuleList = new ArrayList<>();
        DiscountRuleDto newYearDiscount = DiscountRuleDto.builder()
            .ruleId(1)
            .startDate(1640970000000L) // 2022/01/01
            .endDate(1643648399000L) // 2022/01/31
            .discountAmount(50)
            .build();
        discountRuleList.add(newYearDiscount);

        // WHEN
        when(articleService.getAllArticles()).thenReturn(articleDtoList);
        when(discountService.getAvailableRulesByDate(purchaseDate)).thenReturn(discountRuleList);
        GetDisplayItemsResponse response = storeService.getDisplayItems(purchaseDate);

        // GIVEN
        assertEquals(1, response.getReturnCode());
        assertNotNull(response.getDisplayItems());
        assertEquals(1, response.getDisplayItems().size());
        // Discount price
        assertEquals(115, response.getDisplayItems().get(0).getPrice());
        assertEquals("Shoes1", response.getDisplayItems().get(0).getName());
        verify(discountService, times(1)).getAvailableRulesByDate(purchaseDate);
    }

    @Test
    public void testGetDisplayItems_EmptyArticleList() {
        // GIVEN
        long purchaseDate = 1642957200000L; // 2022/01/24
        List<DiscountRuleDto> discountRuleList = new ArrayList<>();
        DiscountRuleDto newYearDiscount = DiscountRuleDto.builder()
            .ruleId(1)
            .startDate(1640970000000L) // 2022/01/01
            .endDate(1643648399000L) // 2022/01/31
            .discountAmount(50)
            .build();
        discountRuleList.add(newYearDiscount);

        // WHEN
        when(articleService.getAllArticles()).thenReturn(new ArrayList<>());
        when(discountService.getAvailableRulesByDate(purchaseDate)).thenReturn(discountRuleList);
        GetDisplayItemsResponse response = storeService.getDisplayItems(purchaseDate);

        // GIVEN
        assertNotNull(response.getDisplayItems());
        assertEquals(0, response.getDisplayItems().size());
    }

    @Test
    public void testGetDisplayItems_EmptyDiscountRuleList() {
        // GIVEN
        long purchaseDate = 1642957200000L; // 2022/01/24

        List<ArticleDto> articleDtoList = new ArrayList<>();
        ArticleDto shoes = new ArticleDto();
        shoes.setName("Shoes1");
        shoes.setSlogan("Best shoes");
        shoes.setNetPrice(100);
        shoes.setSalesPrice(150);
        shoes.setVatRatio(0.1f);
        articleDtoList.add(shoes);

        // WHEN
        when(articleService.getAllArticles()).thenReturn(articleDtoList);
        when(discountService.getAvailableRulesByDate(purchaseDate)).thenReturn(new ArrayList<>());
        GetDisplayItemsResponse response = storeService.getDisplayItems(purchaseDate);

        // GIVEN
        assertNotNull(response.getDisplayItems());
        assertEquals(1, response.getDisplayItems().size());
        // Original price
        assertEquals(165, response.getDisplayItems().get(0).getPrice());
    }

    @Test
    public void testGetDisplayItems_NotInDiscountPeriod() {
        // GIVEN
        long purchaseDate = 1645635600000L; // 2022/02/24

        List<ArticleDto> articleDtoList = new ArrayList<>();
        ArticleDto shoes = new ArticleDto();
        shoes.setName("Shoes1");
        shoes.setSlogan("Best shoes");
        shoes.setNetPrice(100);
        shoes.setSalesPrice(150);
        shoes.setVatRatio(0.1f);
        articleDtoList.add(shoes);

        // WHEN
        when(articleService.getAllArticles()).thenReturn(articleDtoList);
        GetDisplayItemsResponse response = storeService.getDisplayItems(purchaseDate);

        // GIVEN
        assertNotNull(response.getDisplayItems());
        assertEquals(1, response.getDisplayItems().size());
        // Original price
        assertEquals(165, response.getDisplayItems().get(0).getPrice());
    }

    @Test
    public void testGetDisplayItems_DiscountPriceLowerThanNetPrice() {
        // GIVEN
        long purchaseDate = 1642957200000L; // 2022/02/24

        List<ArticleDto> articleDtoList = new ArrayList<>();
        ArticleDto shoes = new ArticleDto();
        shoes.setName("Shoes1");
        shoes.setSlogan("Best shoes");
        shoes.setNetPrice(100);
        shoes.setSalesPrice(150);
        shoes.setVatRatio(0.1f);
        articleDtoList.add(shoes);

        List<DiscountRuleDto> discountRuleList = new ArrayList<>();
        DiscountRuleDto newYearDiscount = DiscountRuleDto.builder()
            .ruleId(1)
            .startDate(1640970000000L) // 2022/01/01
            .endDate(1643648399000L) // 2022/01/31
            .discountAmount(100)
            .build();
        discountRuleList.add(newYearDiscount);

        // WHEN
        when(articleService.getAllArticles()).thenReturn(articleDtoList);
        when(discountService.getAvailableRulesByDate(purchaseDate)).thenReturn(discountRuleList);
        GetDisplayItemsResponse response = storeService.getDisplayItems(purchaseDate);

        // GIVEN
        assertNotNull(response.getDisplayItems());
        assertEquals(1, response.getDisplayItems().size());
        // Original price because discounted price is 65 < net price 100 (total 165 - discount 100)
        assertEquals(165, response.getDisplayItems().get(0).getPrice());
    }

    @Test
    public void testGetDisplayItems_MultipleApplicableDiscountRule() {
        // GIVEN
        long purchaseDate = 1642957200000L; // 2022/02/24

        List<ArticleDto> articleDtoList = new ArrayList<>();
        ArticleDto shoes = new ArticleDto();
        shoes.setName("Shoes1");
        shoes.setSlogan("Best shoes");
        shoes.setNetPrice(100);
        shoes.setSalesPrice(150);
        shoes.setVatRatio(0.1f);
        articleDtoList.add(shoes);

        List<DiscountRuleDto> discountRuleList = new ArrayList<>();
        DiscountRuleDto newYearDiscount = DiscountRuleDto.builder()
            .ruleId(1)
            .startDate(1640970000000L) // 2022/01/01
            .endDate(1643648399000L) // 2022/01/31
            .discountAmount(100)
            .build();

        DiscountRuleDto flashSaleDiscount = DiscountRuleDto.builder()
            .ruleId(1)
            .startDate(1640970000000L) // 2022/01/24
            .endDate(1643129999000L) // 2022/01/25
            .discountAmount(20)
            .build();
        discountRuleList.add(newYearDiscount);
        discountRuleList.add(flashSaleDiscount);

        // WHEN
        when(articleService.getAllArticles()).thenReturn(articleDtoList);
        when(discountService.getAvailableRulesByDate(purchaseDate)).thenReturn(discountRuleList);
        GetDisplayItemsResponse response = storeService.getDisplayItems(purchaseDate);

        // GIVEN
        assertNotNull(response.getDisplayItems());
        assertEquals(1, response.getDisplayItems().size());
        // 2 applicable discount rules but the service will choose the less discount one
        assertEquals(145, response.getDisplayItems().get(0).getPrice());
    }

    @Test
    public void testGetDisplayItems_Exception() {
        // WHEN
        when(articleService.getAllArticles()).thenThrow(NullPointerException.class);
        GetDisplayItemsResponse response = storeService.getDisplayItems(anyLong());

        // GIVEN
        assertEquals(-200, response.getReturnCode());
        assertNotNull(response.getDisplayItems());
        assertEquals(0, response.getDisplayItems().size());
    }

}