package org.store.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.store.dto.DiscountRuleDto;
import org.store.repository.DiscountRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DiscountServiceImpl.class})
public class DiscountServiceImplTest {

    @MockBean private DiscountRepository discountRepository;

    @Autowired private DiscountServiceImpl discountService;

    private DiscountRuleDto discountRuleDto;

    @Before
    public void setUp() {
        discountRuleDto = DiscountRuleDto.builder()
            .ruleId(1)
            .startDate(1640970000000L)
            .endDate(1643648399000L)
            .discountAmount(100)
            .build();
    }

    @Test
    public void testGetAvailableRulesByDate_Successfully() {
        // GIVEN
        List<DiscountRuleDto> expectedList = Collections.singletonList(discountRuleDto);

        //WHEN
        when(discountRepository.getDiscountRulesByDate(anyLong())).thenReturn(expectedList);
        List<DiscountRuleDto> discountRuleDtoList = discountService.getAvailableRulesByDate(anyLong());

        // THEN
        assertNotNull(discountRuleDtoList);
        assertEquals(1, discountRuleDtoList.size());
        verify(discountRepository, times(1)).getDiscountRulesByDate(anyLong());
    }

    @Test
    public void testGetAvailableRulesByDate_NullData() {
        //WHEN
        when(discountRepository.getDiscountRulesByDate(anyLong())).thenReturn(null);
        List<DiscountRuleDto> discountRuleDtoList = discountService.getAvailableRulesByDate(anyLong());

        // THEN
        assertNotNull(discountRuleDtoList);
        verify(discountRepository, times(1)).getDiscountRulesByDate(anyLong());
    }

    @Test
    public void testGetAvailableRulesByDate_Exception() {

        //WHEN
        when(discountRepository.getDiscountRulesByDate(anyLong())).thenThrow(NullPointerException.class);
        List<DiscountRuleDto> discountRuleDtoList = discountService.getAvailableRulesByDate(anyLong());

        // THEN
        assertNotNull(discountRuleDtoList);
        verify(discountRepository, times(1)).getDiscountRulesByDate(anyLong());
    }
}