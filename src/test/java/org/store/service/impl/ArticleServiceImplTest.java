package org.store.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.store.dto.ArticleDto;
import org.store.dto.response.BaseResponse;
import org.store.repository.ArticleRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ArticleServiceImpl.class})
public class ArticleServiceImplTest {

    @MockBean private ArticleRepository articleRepository;

    @Autowired private ArticleServiceImpl articleService;

    private ArticleDto articleDto;

    @Before
    public void setUp() {
        articleDto = new ArticleDto();
        articleDto.setName("shoes");
        articleDto.setSlogan("best shoes");
        articleDto.setNetPrice(100);
        articleDto.setSalesPrice(200);
        articleDto.setVatRatio(0.1f);
    }

    @Test
    public void testAddArticle_Successfully() {
        BaseResponse response = articleService.addArticle(articleDto);

        // THEN
        assertEquals(1, response.getReturnCode());
        verify(articleRepository, times(1)).addArticle(any());
    }

    @Test
    public void testAddArticle_InvalidArticle() {
        // GIVEN
        articleDto.setName("");

        BaseResponse response = articleService.addArticle(articleDto);

        // THEN
        assertEquals(-101, response.getReturnCode());
        verify(articleRepository, times(0)).addArticle(any());
    }

    @Test
    public void testAddArticle_DuplicatedArticle() {
        // GIVEN
        articleDto.setName("shoes");

        // WHEN
        when(articleRepository.checkArticleByName(articleDto.getName())).thenReturn(true);
        BaseResponse response = articleService.addArticle(articleDto);

        // THEN
        assertEquals(-102, response.getReturnCode());
        verify(articleRepository, times(0)).addArticle(any());
    }

    @Test
    public void testAddArticle_Exception() {
        // WHEN
        when(articleRepository.checkArticleByName(articleDto.getName())).thenThrow(NullPointerException.class);

        BaseResponse response = articleService.addArticle(articleDto);

        // THEN
        assertEquals(-100, response.getReturnCode());
        verify(articleRepository, times(0)).addArticle(any());
    }

    @Test
    public void testGetAllArticles_Successfully() {
        // GIVEN
        List<ArticleDto> expectedList = Collections.singletonList(articleDto);

        //WHEN
        when(articleRepository.getArticleList()).thenReturn(Optional.of(expectedList));
        List<ArticleDto> articleDtoList = articleService.getAllArticles();

        // THEN
        assertNotNull(articleDtoList);
        assertEquals(1, articleDtoList.size());
        verify(articleRepository, times(1)).getArticleList();
    }

    @Test
    public void testGetAllArticles_Exception() {

        //WHEN
        when(articleRepository.getArticleList()).thenThrow(NullPointerException.class);
        List<ArticleDto> articleDtoList = articleService.getAllArticles();

        // THEN
        assertNotNull(articleDtoList);
        verify(articleRepository, times(1)).getArticleList();
    }
}