package org.store.service;

import java.util.List;
import org.store.dto.response.GetDisplayItemsResponse;

public interface StoreService {

    GetDisplayItemsResponse getDisplayItems(long purchaseDate);

}
