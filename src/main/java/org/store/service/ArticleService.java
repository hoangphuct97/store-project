package org.store.service;

import java.util.List;
import org.store.dto.ArticleDto;
import org.store.dto.response.BaseResponse;

public interface ArticleService {

    BaseResponse addArticle(ArticleDto articleDto);
    List<ArticleDto> getAllArticles();

}
