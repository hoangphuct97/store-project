package org.store.service.impl;

import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.store.dto.DiscountRuleDto;
import org.store.repository.DiscountRepository;
import org.store.service.DiscountService;

@Slf4j
@RequiredArgsConstructor
@Service
public class DiscountServiceImpl implements DiscountService {
    private final DiscountRepository discountRepository;

    @Override
    public List<DiscountRuleDto> getAvailableRulesByDate(long timestamp) {
        List<DiscountRuleDto> defaultList = new ArrayList<>();
        try {
            List<DiscountRuleDto> result = discountRepository.getDiscountRulesByDate(timestamp);
            if (result == null) {
                return defaultList;
            } else {
                return result;
            }
        } catch (Exception ex) {
            log.error("Get available discount rule failed with exception", ex);
            return defaultList;
        }
    }
}
