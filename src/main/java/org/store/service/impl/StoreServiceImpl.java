package org.store.service.impl;

import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.store.dto.ArticleDto;
import org.store.dto.DiscountRuleDto;
import org.store.dto.DisplayItemDto;
import org.store.dto.response.GetDisplayItemsResponse;
import org.store.service.ArticleService;
import org.store.service.DiscountService;
import org.store.service.StoreService;

@Slf4j
@RequiredArgsConstructor
@Service
public class StoreServiceImpl implements StoreService {
    private final ArticleService articleService;
    private final DiscountService discountService;

    @Override
    public GetDisplayItemsResponse getDisplayItems(long purchaseDate) {
        List<DisplayItemDto> displayItemDtoList = new ArrayList<>();
        GetDisplayItemsResponse response = new GetDisplayItemsResponse();

        try {
            List<ArticleDto> articleList = articleService.getAllArticles();
            List<DiscountRuleDto> discountRuleList = discountService.getAvailableRulesByDate(purchaseDate);

            // Get rule which has lower discount to apply
            DiscountRuleDto discountRule = this.filterDiscountRule(discountRuleList);

            for (ArticleDto article : articleList) {
                double totalPrice = article.getSalesPrice() + article.getSalesPrice()*article.getVatRatio();
                double discountPrice = totalPrice - discountRule.getDiscountAmount();
                double price = discountPrice < article.getNetPrice() ? totalPrice : discountPrice;
                DisplayItemDto displayItemDto = DisplayItemDto.builder()
                    .name(article.getName())
                    .slogan(article.getSlogan())
                    .price(price)
                    .build();
                displayItemDtoList.add(displayItemDto);
            }

            response.setReturnCode(1);
            response.setReturnMessage("Get display items successfully");
            response.setDisplayItems(displayItemDtoList);

        } catch (Exception ex) {
            log.error("Get display items failed with exception", ex);
            response.setReturnCode(-200);
            response.setReturnMessage("Get display items failed with exception");
            response.setDisplayItems(new ArrayList<>());
        }
        return response;
    }

    private DiscountRuleDto filterDiscountRule(List<DiscountRuleDto> discountRuleList) {
        DiscountRuleDto discountRule;
        if (!discountRuleList.isEmpty()) {
            discountRule = discountRuleList
                .stream()
                .reduce(discountRuleList.get(0),
                    (rule1, rule2) ->
                        rule1.getDiscountAmount() < rule2.getDiscountAmount() ? rule1 : rule2);
        } else {
            discountRule = DiscountRuleDto.builder().discountAmount(0).build();
        }
        return discountRule;
    }
}
