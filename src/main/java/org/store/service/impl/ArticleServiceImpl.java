package org.store.service.impl;

import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.store.dto.ArticleDto;
import org.store.dto.response.BaseResponse;
import org.store.repository.ArticleRepository;
import org.store.service.ArticleService;
import org.store.utils.GsonUtil;

@Slf4j
@RequiredArgsConstructor
@Service
public class ArticleServiceImpl implements ArticleService {
    private final ArticleRepository articleRepository;

    @Override
    public BaseResponse addArticle(ArticleDto articleDto) {
        BaseResponse response = new BaseResponse();
        try {
            if (articleDto == null
                || StringUtils.isEmpty(articleDto.getName())
                || articleDto.getNetPrice() <= 0
                || articleDto.getSalesPrice() <= 0
                || articleDto.getVatRatio() <= 0) {

                log.info("Add article fail due to invalid article {}", GsonUtil.toJsonString(articleDto));
                response.setReturnCode(-101);
                response.setReturnMessage("Invalid article");
                return response;
            }

            // check duplicate article;
            if (articleRepository.checkArticleByName(articleDto.getName())) {
                response.setReturnCode(-102);
                response.setReturnMessage("Article already exists");
                return response;
            }

            articleRepository.addArticle(articleDto);
            response.setReturnCode(1);
            response.setReturnMessage("Add article successfully");
            return response;
        } catch (Exception ex) {
            log.error("Add article {} with exception", GsonUtil.toJsonString(articleDto), ex);
            response.setReturnCode(-100);
            response.setReturnMessage("Add article with exception");
            return response;
        }
    }

    @Override
    public List<ArticleDto> getAllArticles() {
        List<ArticleDto> defaultList = new ArrayList<>();
        try {
            // TODO: some logics here when there are new requirements
            return articleRepository.getArticleList().orElse(defaultList);
        } catch (Exception ex) {
            log.error("Get all articles with exception", ex);
            return defaultList;
        }
    }

}
