package org.store.service;

import java.util.List;
import org.store.dto.DiscountRuleDto;

public interface DiscountService {

    List<DiscountRuleDto> getAvailableRulesByDate(long timestamp);

}
