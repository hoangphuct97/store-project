package org.store.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import org.store.dto.ArticleDto;

/**
 * This class should call to the database for data in the future
 */

@Repository
public class ArticleRepository {
    private static final List<ArticleDto> articleDtoList;

    static {
        articleDtoList = new ArrayList<>();
        ArticleDto shoes = new ArticleDto();
        shoes.setName("Shoes1");
        shoes.setSlogan("Best shoes");
        shoes.setNetPrice(100);
        shoes.setSalesPrice(150);
        shoes.setVatRatio(0.1f);

        ArticleDto headphone = new ArticleDto();
        headphone.setName("Headphone1");
        headphone.setSlogan("Best headphone");
        headphone.setNetPrice(300);
        headphone.setSalesPrice(400);
        headphone.setVatRatio(0.2f);

        ArticleDto jacket = new ArticleDto();
        jacket.setName("Jacket1");
        jacket.setSlogan("Best jacket");
        jacket.setNetPrice(200);
        jacket.setSalesPrice(260);
        jacket.setVatRatio(0.15f);

        articleDtoList.add(shoes);
        articleDtoList.add(headphone);
        articleDtoList.add(jacket);
    }

    public Optional<List<ArticleDto>> getArticleList() {
        return Optional.of(articleDtoList);
    }

    public boolean checkArticleByName(String name) {
        return articleDtoList.stream().anyMatch(articleDto -> articleDto.getName().equals(name));
    }

    public void addArticle(ArticleDto articleDto) {
        articleDtoList.add(articleDto);
    }

}
