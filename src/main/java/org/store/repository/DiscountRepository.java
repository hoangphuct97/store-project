package org.store.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Repository;
import org.store.dto.DiscountRuleDto;

/**
 * This class should call to the database for data in the future
 */

@Repository
public class DiscountRepository {
    private static final List<DiscountRuleDto> discountRuleList;

    static {
        discountRuleList = new ArrayList<>();
        /**
         * Discount for users who purchase within New Year event (valid date: 2022/01/01 - 2022/01/31)
         */
        DiscountRuleDto newYearDiscount = DiscountRuleDto.builder()
            .ruleId(1)
            .startDate(1640970000000L)
            .endDate(1643648399000L)
            .discountAmount(50)
            .build();

        /**
         * Discount for users who purchase within Christmas event (valid date: 2021/12/24 - 2021/12/25)
         */
        DiscountRuleDto christmasDiscount = DiscountRuleDto.builder()
            .ruleId(2)
            .startDate(1640278800000L)
            .endDate(1640451599000L)
            .discountAmount(100)
            .build();

        /**
         * Flash sale discount (valid date: 2022/03/24 00:00 - 2022/03/24 23:59)
         */
        DiscountRuleDto flashSale = DiscountRuleDto.builder()
            .ruleId(3)
            .startDate(1648054800000L)
            .endDate(1648141199000L)
            .discountAmount(300)
            .build();

        discountRuleList.add(newYearDiscount);
        discountRuleList.add(christmasDiscount);
        discountRuleList.add(flashSale);
    }

    public List<DiscountRuleDto> getDiscountRulesByDate(long timestamp) {
        return discountRuleList
            .stream()
            .filter(rule -> rule.getStartDate() <= timestamp && timestamp <= rule.getEndDate())
            .collect(Collectors.toList());
    }

}
