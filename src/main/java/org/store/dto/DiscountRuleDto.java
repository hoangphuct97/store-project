package org.store.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class DiscountRuleDto {
    private int ruleId;
    private long startDate;
    private long endDate;
    private long discountAmount;

}
