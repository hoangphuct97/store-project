package org.store.dto.response;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.store.dto.DisplayItemDto;

@Getter
@Setter
public class GetDisplayItemsResponse extends BaseResponse {
    private List<DisplayItemDto> displayItems;

}
