package org.store.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArticleDto {
    private String name;
    private String slogan;
    private long netPrice;
    private long salesPrice;
    private float vatRatio;

}
