package org.store.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class DisplayItemDto {
    private String name;
    private String slogan;
    private double price;

}
