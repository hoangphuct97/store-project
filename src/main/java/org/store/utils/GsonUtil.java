package org.store.utils;

import com.google.gson.*;
public class GsonUtil {

  private static final Gson gson;

  static {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    gson = gsonBuilder.disableHtmlEscaping().create();
  }

  public static String toJsonString(Object obj) {
    if (!(obj instanceof String)) {
      return gson.toJson(obj);
    }
    return (String) obj;
  }

  public static <T> T fromJsonString(String sJson, Class<T> t) {
    return gson.fromJson(sJson, t);
  }


}
