package org.store.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.store.dto.ArticleDto;
import org.store.dto.response.BaseResponse;
import org.store.dto.response.GetDisplayItemsResponse;
import org.store.service.ArticleService;
import org.store.service.StoreService;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "api")
public class StoreController {
    private final ArticleService articleService;
    private final StoreService storeService;

    @PostMapping("article")
    public ResponseEntity<BaseResponse> addArticle(ArticleDto articleDto) {
        long startTime = System.currentTimeMillis();
        log.info("Start to add article");
        try {
            return ResponseEntity.ok(articleService.addArticle(articleDto));
        } finally {
            log.info("Adding article has finished, execution time {} ms",
                System.currentTimeMillis() - startTime);
        }
    }

    @GetMapping("article")
    public ResponseEntity<GetDisplayItemsResponse> getArticle(@RequestParam(name = "purchase_date") long purchaseDate) {
        long startTime = System.currentTimeMillis();
        log.info("Start to get article from storage");
        try {
            return ResponseEntity.ok(storeService.getDisplayItems(purchaseDate));
        } finally {
            log.info("Querying and calculating articles has finished, execution time {} ms",
                System.currentTimeMillis() - startTime);
        }
    }

}
