# Store Application

Store is an application that was built on Spring framework. Store would provide a list of articles with a discounted price. Also, Store allows users to add a new article.

## Specification
- A StoreController will take over the requests and call the service layer to handle then response to clients with execution time.
- 3 Services: ArticleService, DiscountRuleService and StoreService which follow **Proxy and Dependency Injection** design pattern, **SOLID and DRY** principles. **StoreService** calls **ArticleService** to get articles, then calls **DiscountRuleService** to get discount rules and builds a list of displayed items.
- Price without discount formula:
> salesPrice + salesPrice * vatRatio
- Price with discount formula:
> (salesPrice + salesPrice * vatRatio) - discountAmount
- If there are many discount rules which are applicable, Store will choose the less discountAmount rule.
- If discountPrice is lower than netPrice, Store will return price without discount.
- **This project also has some unit tests with 100% coverage for the service layer.**

## Tech stacks
- Spring Boot
- Java 8
- Junit & Mockito

## How to run

- Use an IDE to import Spring project then run the application.
- Or use Gitlab link below to clone the project and run the application.
- Application port: **8788**

## API specs and cURL:
- GET ***/api/article***, param **purchase_date** (timestamp): get a list of articles with discount price.
```bash
curl --location --request GET 'http://localhost:8788/api/article?purchase_date=xxx'
```
- POST ***/api/article***, body **form-urlencoded**: add new article.
```bash
curl --location --request POST 'http://localhost:8788/api/article' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'name=shoes' \
--data-urlencode 'netPrice=1000' \
--data-urlencode 'salesPrice=1500' \
--data-urlencode 'vatRatio=0.3'
```

## Gitlab for this project
[https://gitlab.com/hoangphuct97/store-project](https://gitlab.com/hoangphuct97/store-project)